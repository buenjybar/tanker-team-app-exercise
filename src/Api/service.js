import _data from './data.json'

const cleanUpTeamData = (team) => ({
    id: team.id,
    userReference: team.user_id,
    userInTeam: team.members_id
});

const TIMEOUT = 500;

export default {
    getCurrentUser: (cb, timeout) => setTimeout(() => cb(_data['current_user']), timeout || TIMEOUT),
    getMembers: (cb, timeout) => setTimeout(() => cb(_data['users']), timeout || TIMEOUT),
    getTeams: (cb, timeout) => setTimeout(() => cb(_data['teams'].map(cleanUpTeamData)), timeout || TIMEOUT),
}
