import reducer from './selecteduser';
import * as types from '../Constants/actiontypes';

describe('Search Filter reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(-1)
    });

    it('should handle SET_SEARCH_FILTER', () => {

        expect(reducer([], {
            type: types.SET_SELECTED_USER,
            selectedUser: 0
        })).toEqual(0);

        const text = 0;
        expect(reducer([], {
            type: types.SET_SELECTED_USER,
            selectedUser: text
        })).toEqual(text);

        const text2 = -1;
        expect(reducer([], {
            type: types.SET_SELECTED_USER,
            selectedUser: text2
        })).toEqual(text2);

    });
});