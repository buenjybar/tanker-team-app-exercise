import reducer from './team';
import * as types from '../Constants/actiontypes';

import Team from '../Models/team';
import User from '../Models/user';

describe('team reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual([])
    });

    it('should handle ADD_TEAM', () => {

        const team = new Team();
        const team2 = new Team();

        expect(reducer([], {
            type: types.ADD_TEAM,
            team: team
        })).toEqual([team]);

        expect(reducer([team], {
            type: types.ADD_TEAM,
            team: team2
        })).toEqual([team, team2])
    });


    it('should handle ADD_MEMBER_TO_TEAM', () => {

        const team = new Team();
        const user = new User();
        const team2 = new Team();

        expect(reducer([], {
            type: types.ADD_MEMBER_TO_TEAM,
            team: team,
            user: user
        })).toEqual([team]);

        expect(reducer([], {
            type: types.ADD_MEMBER_TO_TEAM,
            team: team2,
            user: user
        })).toEqual([team2])
    });


    it('should handle REMOVE_MEMBER_TO_TEAM', () => {

        const team = new Team();
        const user = new User();
        const team2 = new Team();

        expect(reducer([], {
            type: types.REMOVE_MEMBER_TO_TEAM,
            team: team,
            user: user
        })).toEqual([team]);

        expect(reducer([], {
            type: types.REMOVE_MEMBER_TO_TEAM,
            team: team2,
            user: user
        })).toEqual([team2])
    });



});