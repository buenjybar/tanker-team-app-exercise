import reducer from './members';
import * as types from '../Constants/actiontypes';

import User from '../Models/User';

describe('member reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual([])
    });

    it('should handle ADD_MEMBER', () => {

        const user = new User();
        const user2 = new User();

        expect(reducer([], {
            type: types.ADD_MEMBER,
            user: user
        })).toEqual([user]);

        expect(reducer([user], {
            type: types.ADD_MEMBER,
            user: user2
        })).toEqual([user, user2])
    })
});