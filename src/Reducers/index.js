import {combineReducers} from 'redux';
import members from './members';
import teams from './team';
import searchFilter from './searchfilter'
import selectedUser from './selecteduser'

const reducers = combineReducers({
    members,
    teams,
    selectedUser,
    searchFilter
});

export default reducers;