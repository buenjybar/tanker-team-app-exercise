import TeamModel from '../Models/team';
import * as types from '../Constants/actiontypes';

/**
 * TeamModel Reducer
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const team = (state, action) => {
    switch (action.type) {
        case types.ADD_TEAM:
            return new TeamModel(action.team);

        case types.ADD_MEMBER_TO_TEAM:
            let userToAdd = action.user;

            let currentUsers = action.team.userInTeam;
            if (currentUsers.indexOf(userToAdd.id) === -1) {
                currentUsers.push(userToAdd.id); // add new user to team association
            }

            return new TeamModel({
                id: action.team.id,
                userReference: action.team.userReference,
                userInTeam: currentUsers
            });

        case types.REMOVE_MEMBER_TO_TEAM:
            let userToRemove = action.user;

            let users = action.team.userInTeam;
            let index = users.indexOf(userToRemove.id);
            if (index !== -1) {
                users.splice(index, 1); // remove user to team association
            }

            return new TeamModel({
                id: action.team.id,
                userReference: action.team.userReference,
                userInTeam: users
            });

        default:
            return state;
    }
};

/**
 * Teams Reducer
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const teams = (state = [], action) => {
    switch (action.type) {
        case types.ADD_TEAM:
            return [
                ...state,
                team(undefined, action)
            ];

        case types.ADD_MEMBER_TO_TEAM:

            state = state.filter((teams) => teams.id !== action.team.id);

            return [
                ...state,
                team(undefined, action)
            ];

        case types.REMOVE_MEMBER_TO_TEAM:

            state = state.filter((teams) => teams.id !== action.team.id);

            return [
                ...state,
                team(undefined, action)
            ];
        default:
            return state;
    }
};

export default teams;