import * as types from '../Constants/actiontypes';

const initialState = -1;

/**
 * Selected user Reducer
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const selectedUser = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_SELECTED_USER:
            return action.selectedUser;
        default:
            return state;
    }
};

export default selectedUser;