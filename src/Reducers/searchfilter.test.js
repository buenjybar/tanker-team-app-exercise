import reducer from './searchfilter';
import * as types from '../Constants/actiontypes';

describe('Search Filter reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual('')
    });

    it('should handle SET_SEARCH_FILTER', () => {

        expect(reducer([], {
            type: types.SET_SEARCH_FILTER,
            filter: ''
        })).toEqual('');

        const text = 'this is a test string';
        expect(reducer([], {
            type: types.SET_SEARCH_FILTER,
            filter: text
        })).toEqual(text);

        const text2 = null;
        expect(reducer([], {
            type: types.SET_SEARCH_FILTER,
            filter: text2
        })).toEqual(text2);

    });
});