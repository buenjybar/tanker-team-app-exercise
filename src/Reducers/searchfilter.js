import * as types from '../Constants/actiontypes';

/**
 * search filter reducer
 *
 * @param {string} state
 * @param {object} action
 * @returns {object}
 */
const searchFilter = (state = '', action) => {
    switch (action.type) {
        case types.SET_SEARCH_FILTER:
            return action.filter;
        default:
            return state;
    }
};

export default searchFilter;