import UserModel from '../Models/user';
import * as types from '../Constants/actiontypes';

/**
 * Member Reducer
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const member = (state, action) => {
    switch (action.type) {
        case types.ADD_MEMBER:
            return new UserModel(action.user);
        default:
            return state;
    }
};

/**
 * Members Reducer
 * @param {object} state
 * @param {object} action
 * @returns {object}
 */
const members = (state = [], action) => {
    switch (action.type) {
        case types.ADD_MEMBER:
            return [
                ...state,
                member(undefined, action)
            ];
        default:
            return state;
    }
};

export default members;