import * as actions from './index';
import * as types from '../Constants/actiontypes';

import User from '../Models/User';
import Team from '../Models/Team';

describe('actions', () => {
    it('should add user to a team', () => {
        const user = new User({});
        const team = new Team({});

        const expectedAction = {
            type: types.ADD_MEMBER_TO_TEAM,
            user: user,
            team: team
        };
        expect(actions.addUserToTeam(user, team)).toEqual(expectedAction);
    });

    it('should remove user to a team', () => {
        const user = new User({});
        const team = new Team({});

        const expectedAction = {
            type: types.REMOVE_MEMBER_TO_TEAM,
            user: user,
            team: team
        };
        expect(actions.removeUserToTeam(user, team)).toEqual(expectedAction);
    });


    it('should add a user', () => {
        const user = new User({});

        const expectedAction = {
            type: types.ADD_MEMBER,
            user: user
        };
        expect(actions.addUser(user)).toEqual(expectedAction);
    });

    it('should add a team', () => {
        const team = new Team({});

        const expectedAction = {
            type: types.ADD_TEAM,
            team: team
        };
        expect(actions.addTeam(team)).toEqual(expectedAction);
    });

    it('should search', () => {
        const text = ' This is a search test ';

        const expectedAction = {
            type: types.SET_SEARCH_FILTER,
            filter: text
        };
        expect(actions.setSearchFilter(text)).toEqual(expectedAction);
    });

    it('should select user', () => {
        const userId = 0;

        const expectedAction = {
            type: types.SET_SELECTED_USER,
            selectedUser: userId
        };
        expect(actions.selectedUser(userId)).toEqual(expectedAction);
    });

});