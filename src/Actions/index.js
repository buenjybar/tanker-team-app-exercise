import service from '../Api/service';
import * as types from '../Constants/actiontypes';

export const addUserToTeam = (user, team) => ({
    type: types.ADD_MEMBER_TO_TEAM,
    user: user,
    team: team
});

export const removeUserToTeam = (user, team) => ({
    type: types.REMOVE_MEMBER_TO_TEAM,
    user: user,
    team: team
});

export const addUser = (user) => ({
    type: types.ADD_MEMBER,
    user: user
});

export const addTeam = (team) => ({
    type: types.ADD_TEAM,
    team: team
});

export const setSearchFilter = (filter) => ({
    type: types.SET_SEARCH_FILTER,
    filter: filter
});

export const getAllMembers = () => (dispatch) => {
    service.getMembers((users) => {
        users.map((user) => dispatch(addUser(user)));
    });
};

export const getAllTeams = () => (dispatch) => {
    service.getTeams((teams) => {
        teams.map((team) => dispatch(addTeam(team)));
    });
};

export const selectedUser = (userid) => ({
    type: types.SET_SELECTED_USER,
    selectedUser: userid
});