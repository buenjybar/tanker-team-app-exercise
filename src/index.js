import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import reducer from './Reducers';
import thunk from 'redux-thunk';

import {getAllMembers, getAllTeams, selectedUser} from './Actions';

import App from './App';
import './index.css';

const middleware = [thunk];

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);

// /* DEBUGGER */
// store.subscribe(() =>
//     console.log(store.getState())
// );

store.dispatch(getAllMembers());
store.dispatch(getAllTeams());
store.dispatch(selectedUser(4));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);