import React from 'react';
import './team.css';

import User from '../User/user';

import {connect} from 'react-redux';
import {setSearchFilter} from '../../Actions';

const mapStateToProps = state => ({
    items: state.members,
    teams: state.teams,
    selectedUser: state.selectedUser,
    searchValue: state.searchFilter
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    search: (value) => dispatch(setSearchFilter(value))
});

class Team extends React.Component {

    onHandleChange(e) {
        e.stopPropagation();

        let value = e.target.value;

        // set the SearchString state
        this.props.search(value);
    }


    renderUser(item) {
        return (
            <User key={item.id} user={item}>
            </User>
        );
    }

    renderEmptyResult() {
        return (
            <div className="centered">
                <em> No data received. </em>
            </div>
        )
    }

    render() {

        let allUsers = this.props.items || [];
        let searchStr = this.props.searchValue || '';

        const matchingStr = searchStr.trim().toLowerCase();

        if (searchStr.length > 0) {
            allUsers = allUsers.filter((item) => {
                return item && item.getName().toLowerCase().match(matchingStr);
            });
        }
        const items = allUsers.length > 0 ? allUsers.map((item) => this.renderUser(item)) : this.renderEmptyResult();

        return (
            <div className="team-component centered">

                <div className="team-component-header">

                    <div className="team-component-header-left">

                        <h3 className="team-component-header__title">
                            Members In My Team
                        </h3>

                    </div>

                    <div className="team-component-header-right">

                        <div className="search-box-input">

                            <span className="search-box-input__icon">
                                <i className="mdi mdi-magnify"></i>
                            </span>

                            <input type="text"
                                   value={ searchStr }
                                   onChange={ this.onHandleChange.bind(this) }
                                   placeholder="Search..."
                                   className="search-box-input search-box-input__text expand-animation"/>

                        </div>

                    </div>

                </div>

                <div className="team-component-list">
                    { items }
                </div>

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Team);