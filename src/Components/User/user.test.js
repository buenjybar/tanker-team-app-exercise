import React from 'react'
import {shallow} from 'enzyme';

import connectedUser, {User} from './user';
import TeamModel from '../../Models/team';
import UserModel  from '../../Models/user';

function setup(user) {
    const props = {
        user: user,
        teams: [new TeamModel({userReference: 1, userInTeam: [0]})],
        selectedUser: 1
    };

    const enzymeWrapper = shallow(<User {...props} />);

    return {
        props,
        enzymeWrapper
    }
}

describe('components', () => {
    describe('User', () => {
        it('should render self as selectedUser', () => {
            const user = new UserModel({id: 1, firstName: 'tester', lastName: 'TESTED', email: 'example@tester.test'});

            const {enzymeWrapper} = setup(user);

            const userComponent = enzymeWrapper.find('div');
            const leftUserComponent = enzymeWrapper.find('div').find('div.user-component-left');
            const centerUserComponent = enzymeWrapper.find('div').find('div.user-component-center');
            const rightUserComponent = enzymeWrapper.find('div').find('div.user-component-right');

            expect(userComponent).not.toBe(null);
            expect(leftUserComponent).not.toBe(null);
            expect(centerUserComponent).not.toBe(null);
            expect(rightUserComponent).not.toBe(null);

            //current user should be highlighted
            expect(leftUserComponent.find('div.user-initials').hasClass('highlighted')).toBe(true);
            expect(leftUserComponent.find('div.user-initials').text()).toBe(user.getInitials());

            expect(centerUserComponent.find('span.user-name')).not.toBe(null);
            expect(centerUserComponent.find('span.user-name').text()).toBe(user.getName());

            expect(centerUserComponent.find('span.user-email')).not.toBe(null);
            expect(centerUserComponent.find('span.user-email').text()).toBe(user.getEmail());
        });


        it('should render self as a team member', () => {
            const user = new UserModel({
                id: 0,
                firstName: 'tester2',
                lastName: 'TESTED2',
                email: 'example2@tester.test'
            });

            const {enzymeWrapper} = setup(user);

            const userComponent = enzymeWrapper.find('div');
            const leftUserComponent = enzymeWrapper.find('div').find('div.user-component-left');
            const centerUserComponent = enzymeWrapper.find('div').find('div.user-component-center');
            const rightUserComponent = enzymeWrapper.find('div').find('div.user-component-right');

            expect(userComponent).not.toBe(null);
            expect(leftUserComponent).not.toBe(null);
            expect(centerUserComponent).not.toBe(null);
            expect(rightUserComponent).not.toBe(null);

            //current user should be highlighted
            expect(leftUserComponent.find('div.user-initials').hasClass('highlighted')).toBe(true);
            expect(leftUserComponent.find('div.user-initials').text()).toBe(user.getInitials());

            expect(centerUserComponent.find('span.user-name')).not.toBe(null);
            expect(centerUserComponent.find('span.user-name').text()).toBe(user.getName());

            expect(centerUserComponent.find('span.user-email')).not.toBe(null);
            expect(centerUserComponent.find('span.user-email').text()).toBe(user.getEmail());
        });


        it('should render self as an other user', () => {
            const user = new UserModel({
                id: 2,
                firstName: 'tester3',
                lastName: 'TESTED3',
                email: 'example3@tester.test'
            });

            const {enzymeWrapper} = setup(user);

            const userComponent = enzymeWrapper.find('div');
            const leftUserComponent = enzymeWrapper.find('div').find('div.user-component-left');
            const centerUserComponent = enzymeWrapper.find('div').find('div.user-component-center');
            const rightUserComponent = enzymeWrapper.find('div').find('div.user-component-right');

            expect(userComponent).not.toBe(null);
            expect(leftUserComponent).not.toBe(null);
            expect(centerUserComponent).not.toBe(null);
            expect(rightUserComponent).not.toBe(null);

            //current user should NOT to be highlighted
            expect(leftUserComponent.find('div.user-initials').hasClass('highlighted')).toBe(false);
            expect(leftUserComponent.find('div.user-initials').text()).toBe(user.getInitials());

            expect(centerUserComponent.find('span.user-name')).not.toBe(null);
            expect(centerUserComponent.find('span.user-name').text()).toBe(user.getName());

            expect(centerUserComponent.find('span.user-email')).not.toBe(null);
            expect(centerUserComponent.find('span.user-email').text()).toBe(user.getEmail());
        });
    })
})