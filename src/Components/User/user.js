import React from 'react';
import {connect} from 'react-redux';
import {setSearchFilter} from '../../Actions';

import './user.css';

import TeamModel from '../../Models/team';
import Invite from '../Actions/invite';
import Empty from '../Actions/empty';
import Reset from '../Actions/reset';

const mapStateToProps = state => ({
    teams: state.teams,
    selectedUser: state.selectedUser
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    search: (value) => dispatch(setSearchFilter(value))
});


export class User extends React.Component {

    render() {

        const selectedUser = this.props.selectedUser || null;
        const allTeams = this.props.teams || [];

        const currentTeam = allTeams.filter((team) => selectedUser === team.userReference)[0] || null;

        const user = this.props.user;
        const isUserReference = selectedUser === user.id;
        const isAlreadyACurrentTeamMember = currentTeam && currentTeam instanceof TeamModel ? currentTeam.isUserInTeam(user) : false;

        const initials = user.getInitials();
        const email = user.getEmail();
        const name = user.getName();

        return (
            <div className="user-component">

                <div className="user-component-left">

                    <div
                        className={"user-initials" + (isAlreadyACurrentTeamMember || isUserReference ? " highlighted" : "")}>
                        { initials }
                    </div>

                </div>


                <div className="user-component-center">
                    <span className="user-name">
                        { name }
                    </span>

                    <br/>

                    <span className="user-email">
                        { email }
                    </span>
                </div>

                <div className="user-component-right">

                    {isUserReference && !isAlreadyACurrentTeamMember && <Empty user={user} team={currentTeam}/>}

                    {isAlreadyACurrentTeamMember && !isUserReference && <Reset user={user} team={currentTeam}/>}

                    {!isUserReference && !isAlreadyACurrentTeamMember && <Invite user={user} team={currentTeam}/>}

                </div>

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User)