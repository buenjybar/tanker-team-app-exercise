import React from 'react';
import {connect} from 'react-redux';
import {addUserToTeam} from '../../Actions';

const mapStateToProps = state => ({});

const mapDispatchToProps = (dispatch, ownProps) => ({
    addUser: (user, team) => dispatch(addUserToTeam(user, team))
});


class Invite extends React.Component {

    handleClick(e) {
        e.stopPropagation();

        this.props.addUser(this.props.user, this.props.team);
    }

    render() {

        return (
            <div className="user-action clickable" onClick={this.handleClick.bind(this)}>
                <i className="mdi mdi-account-plus"></i>
                <span className="user-action__text"> Invite </span>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invite);