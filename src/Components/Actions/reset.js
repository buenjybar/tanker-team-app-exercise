import React from 'react';
import {connect} from 'react-redux';
import {removeUserToTeam} from '../../Actions';

const mapStateToProps = state => ({});

const mapDispatchToProps = (dispatch, ownProps) => ({
    removeUser: (user, team) => dispatch(removeUserToTeam(user, team))
});

class Reset extends React.Component {

    handleClick(e) {
        e.stopPropagation();

        this.props.removeUser(this.props.user, this.props.team);
    }

    render(){
        return (
            <div className="user-action clickable"  onClick={this.handleClick.bind(this)}>
                <i className="mdi mdi-account-remove"></i>
                <span className="user-action__text"> Reset </span>
            </div>
        )
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Reset);