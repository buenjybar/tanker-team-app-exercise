/**
 * TeamModel Model Class
 */
class TeamModel {
    id;
    userReference;
    userInTeam;

    constructor(item) {
        if (!item) item = {};
        this.id = item.id;
        this.userReference = item.userReference || '';
        this.userInTeam = item.userInTeam || [];
    }

    /**
     * Check if the specified user is part of this team.
     * @param {User} user
     * @returns {boolean}
     */
    isUserInTeam(user){
        return this.userInTeam.indexOf(user.id) !== -1;
    }

}

export default TeamModel;