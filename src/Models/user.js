/**
 * User Model Class
 */
class UserModel {
    id;
    firstName;
    lastName;
    emailAddress;

    constructor(item) {
        if (!item) item = {};
        this.id = item.id;
        this.firstName = item.firstName || '';
        this.lastName = item.lastName || '';
        this.emailAddress = item.emailAddress || '';
    }

    /**
     * Get User Initials
     * @returns {string}
     */
    getInitials() {
        return this.firstName.charAt(0) + this.lastName.charAt(0);
    }

    /**
     * Get User name
     * @returns {string}
     */
    getName() {
        return this.firstName + ' ' + this.lastName;
    }

    /**
     * Get User email
     * @returns {string}
     */
    getEmail() {
        return this.emailAddress;
    }
}

export default  UserModel;