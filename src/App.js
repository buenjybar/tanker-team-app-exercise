import React, {Component} from 'react';
import './App.css';

import Team from './Components/Team/team';

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Tanker Exercise - Benjamin Barre</h2>
                </div>
                <Team></Team>
            </div>
        );
    }
}

export default App;
